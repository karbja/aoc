class Monkey:
    def __init__(self, name, items, operator, operand2, divisible):
        self.name = name
        self.items = items
        self.operator = operator
        self.operand2 = operand2
        self.monkey_true = ...
        self.monkey_false = ...
        self.divisible = divisible
        self.cnt = 0

    def setmonkeys(self, monkey_true, monkey_false):
        self.monkey_true = monkey_true
        self.monkey_false = monkey_false

    def setitem(self, item_value):
        self.items.append(item_value)

    def deleteitem(self):
        self.items.pop(0)

    def getmonkey(self):
        if self.items.__len__() > 0:
            self.cnt += 1
            item_value = self.items[0]
            print("Monkey inspects an item with a worry level of " + str(item_value))
            if self.operator == "+":
                item_value = item_value + self.operand2
                print("Worry level is increased by " + str(self.operand2) + " to " + str(item_value))
            elif self.operator == "*":
                if self.operand2 == "old":
                    old = item_value
                    item_value = old * old
                    print("Worry level is multiplied by " + str(old) + " to " + str(item_value))
                else:
                    item_value = item_value * self.operand2
                    print("Worry level is multiplied by " + str(self.operand2) + " to " + str(item_value))
            item_value = item_value // 3
            print("Monkey gets bored with item. Worry level is divided by 3 to " + (str(item_value)))
            if (item_value / self.divisible).is_integer():
                self.monkey_true.setitem(item_value)
                print("Item is thrown to " + self.monkey_true.name)
            else:
                self.monkey_false.setitem(item_value)
                print("Item is thrown to " + self.monkey_false.name)
            self.deleteitem()


# def __init__(self, name, items, operator, operand2):
monkey0 = Monkey("Monkey0", [85, 79, 63, 72], "*", 17, 2)
monkey1 = Monkey("Monkey1", [53, 94, 65, 81, 93, 73, 57, 92], "*", "old", 7)
monkey2 = Monkey("Monkey2", [62, 63], "+", 7, 13)
monkey3 = Monkey("Monkey3", [57, 92, 56], "+", 4, 5)
monkey4 = Monkey("Monkey4", [67], "+", 5, 3)
monkey5 = Monkey("Monkey5", [85, 56, 66, 72, 57, 99], "+", 6, 19)
monkey6 = Monkey("Monkey6", [86, 65, 98, 97, 69], "*", 13, 11)
monkey7 = Monkey("Monkey7", [87, 68, 92, 66, 91, 50, 68], "+", 2, 17)
monkey0.setmonkeys(monkey2, monkey6)
monkey1.setmonkeys(monkey0, monkey2)
monkey2.setmonkeys(monkey7, monkey6)
monkey3.setmonkeys(monkey4, monkey5)
monkey4.setmonkeys(monkey1, monkey5)
monkey5.setmonkeys(monkey1, monkey0)
monkey6.setmonkeys(monkey3, monkey7)
monkey7.setmonkeys(monkey4, monkey3)

monkeylist = [monkey0, monkey1, monkey2, monkey3, monkey4, monkey5, monkey6, monkey7]
for i in range(20):
    for monkeyx in monkeylist:
        print(monkeyx.name)
        print("----------------")
        monkey_range = monkeyx.items.__len__()
        for item in range(monkey_range):
            monkeyx.getmonkey()

inspection_rate = []
for monkeyx in monkeylist:
    print(monkeyx.items)
    print(monkeyx.name + " inspected items " + str(monkeyx.cnt) + " times")
    inspection_rate.append(monkeyx.cnt)

inspection_rate.sort(reverse=True)
print(inspection_rate[0] * inspection_rate[1])


