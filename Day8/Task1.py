forest = []
max = 99
rows, cols = max, max
for i in range(rows):
    col = []
    for j in range(cols):
        col.append(0)
    forest.append(col)

f = open("puzzle.txt", "r")
lines = f.readlines()
x = 0
y = 0
cnt = 0
for line in lines:
    for letter in line:
        if letter.isdigit() is False:
            continue
        forest[x][y] = int(letter)
        y += 1
    y = 0
    x += 1
print(forest)

cnt = 0
x = 0
y = 0
higher = True
for i in range(max):
    for j in range(max):
        x = i
        y = j
        while higher:
            print("X: " + str(x) + " Y: " + str(y))
            print(forest[x][y])
            if i != 0 and i != max-1 and j != 0 and j != max - 1:
                if forest[x-1][y] >= forest[i][j]:
                    break
                else:
                    x -= 1
                if x == 0:
                    higher = False
            else:
                higher = False
        x = i
        while higher:
            print("X: " + str(x) + " Y: " + str(y))
            print(forest[x][y])
            if i != 0 and i != max - 1 and j != 0 and j != max - 1:
                if forest[x + 1][y] >= forest[i][j]:
                    break
                else:
                    x += 1
                if x == max - 1:
                    higher = False
            else:
                higher = False
        x = i
        while higher:
            print("X: " + str(x) + " Y: " + str(y))
            print(forest[x][y])
            if i != 0 and i != max - 1 and j != 0 and j != max - 1:
                if forest[x][y - 1] >= forest[i][j]:
                    break
                else:
                    y -= 1
                if y == 0:
                    higher = False
            else:
                higher = False
        y = j
        while higher:
            print("X: " + str(x) + " Y: " + str(y))
            print(forest[x][y])
            if i != 0 and i != max - 1 and j != 0 and j != max - 1:
                if forest[x][y + 1] >= forest[i][j]:
                    break
                else:
                    y += 1
                if y == max-1:
                    higher = False
            else:
                higher = False

        if higher is False:
            print("This one!")
            cnt += 1
        higher = True
        print("----------------")

print(cnt)