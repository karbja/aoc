forest = []
max = 99
rows, cols = max, max
for i in range(rows):
    col = []
    for j in range(cols):
        col.append(0)
    forest.append(col)

f = open("puzzle.txt", "r")
lines = f.readlines()
x = 0
y = 0
cnt = 0
for line in lines:
    for letter in line:
        if letter.isdigit() is False:
            continue
        forest[x][y] = int(letter)
        y += 1
    y = 0
    x += 1

tree_score = 0
max_value = 0
x = 0
y = 0
a = 0
b = 0
c = 0
d = 0
cycle = True
for i in range(max):
    for j in range(max):
        x = i
        y = j
        while cycle:
            if x > 0:
                a += 1
                if forest[x-1][y] >= forest[i][j]:
                    cycle = False
                x -= 1
            else:
                cycle = False
        x = i
        cycle = True
        while cycle:
            if x < max-1:
                b += 1
                if forest[x+1][y] >= forest[i][j]:
                    cycle = False
                x += 1
            else:
                cycle = False
        x = i
        cycle = True
        while cycle:
            if y > 0:
                c += 1
                if forest[x][y-1] >= forest[i][j]:
                    cycle = False
                y -= 1
            else:
                cycle = False
        y = j
        cycle = True
        while cycle:
            if y < max-1:
                d += 1
                if forest[x][y+1] >= forest[i][j]:
                    cycle = False
                y += 1
            else:
                cycle = False
        cycle = True
        tree_score = a * b * c * d
        if max_value < tree_score:
            max_value = tree_score
        tree_score = 0
        a, b, c, d = 0, 0, 0, 0
print(max_value)
