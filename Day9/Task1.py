from dataclasses import dataclass


@dataclass
class Point:
    x: int
    y: int


def check_position(tailx, headx, head_last_stepx):
    if (abs(tailx.x - headx.x) == 2) or (abs(tailx.y - headx.y) == 2):
        tailx.x = head_last_stepx.x
        tailx.y = head_last_stepx.y
    return tailx


def step(headx, directionx):
    if directionx == "L":
        headx.x -= 1
    elif directionx == "R":
        headx.x += 1
    elif directionx == "D":
        headx.y -= 1
    elif directionx == "U":
        headx.y += 1
    return headx


head = Point(0, 0)
head_last_step = Point(0, 0)
tail = Point(0, 0)
direction = ""
move = 0
point_list = []

f = open("puzzle.txt", "r")
lines = f.readlines()
for line in lines:
    direction = line.split(" ")[0]
    move = line.split(" ")[1]
    for i in range(int(move)):
        head_last_step.x = head.x
        head_last_step.y = head.y
        head = step(head, direction)
        tail = check_position(tail, head, head_last_step)
        if not point_list.__contains__((tail.x, tail.y)):
            point_list.append((tail.x, tail.y))
print(point_list.__len__())
