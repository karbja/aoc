counter = 0
max_val1 = 0
min_val1 = 0
max_val2 = 0
min_val2 = 0
# parsování vstupního řádku a přiřazení hodnot
f = open("puzzle.txt", "r")
lines = f.readlines()
for line in lines:
    # parsování vstupního řádku a přiřazení hodnot
    first_part = line.split(",")[0]
    second_part = line.split(",")[1]
    min_val1 = int(first_part.split("-")[0])
    max_val1 = int(first_part.split("-")[1])
    min_val2 = int(second_part.split("-")[0])
    max_val2 = int(second_part.split("-")[1].split("\n")[0])
    if ((min_val1 <= min_val2 and min_val2 <= max_val1) or (min_val1 <= max_val2 and max_val2 <= max_val1) or
        (min_val2 <= min_val1 and min_val1 <= max_val2) or (min_val2 <= max_val1 and max_val1 <= max_val2)):
        counter += 1
print(counter)
