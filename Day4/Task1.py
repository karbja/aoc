counter = 0
max_val1 = 0
min_val1 = 0
max_val2 = 0
min_val2 = 0
# parsování vstupního řádku a přiřazení hodnot
f = open("puzzle.txt", "r")
lines = f.readlines()
for line in lines:
    # parsování vstupního řádku a přiřazení hodnot
    first_part = line.split(",")[0]
    second_part = line.split(",")[1]
    min_val1 = first_part.split("-")[0]
    max_val1 = first_part.split("-")[1]
    min_val2 = second_part.split("-")[0]
    max_val2 = second_part.split("-")[1].split("\n")[0]
    if (int(max_val1) >= int(max_val2) and int(min_val1) <= int(min_val2)) or (int(max_val2) >= int(max_val1) and int(min_val2) <= int(min_val1)):
        counter += 1
print(counter)
