def check(clock_val, register_val):
    if clock_val == 20 or clock_val == 60 or clock_val == 100\
            or clock_val == 140 or clock_val == 180 or clock_val == 220:
        return clock_val * register_val
    return 0


f = open("puzzle.txt", "r")
lines = f.readlines()
clock = 0
register_x = 1
register_sum = 0
for line in lines:
    if line == "noop\n":
        clock += 1
        register_sum += check(clock, register_x)
    elif line.__contains__("addx "):
        clock += 1
        register_sum += check(clock, register_x)
        clock += 1
        register_sum += check(clock, register_x)
        tmp_sum = line.split(" ")[1].split("\n")[0]
        register_x += int(tmp_sum)
print(register_sum)
