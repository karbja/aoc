def draw(clock_val, sprite_val):
    print("CRT draws pixel in position: " + str(clock_val-1) + " and sprite position is: " + str(sprite_val))
    if 201 <= clock_val <= 240:
        if (clock_val-201 == sprite_val - 1) or (clock_val - 201 == sprite_val) or (clock_val - 201 == sprite_val + 1):
            drawing6.append("#")
        else:
            drawing6.append(".")
    if 161 <= clock_val <= 200:
        if (clock_val-161 == sprite_val - 1) or (clock_val - 161 == sprite_val) or (clock_val - 161 == sprite_val + 1):
            drawing5.append("#")
        else:
            drawing5.append(".")
    if 121 <= clock_val <= 160:
        if (clock_val-121 == sprite_val - 1) or (clock_val - 121 == sprite_val) or (clock_val - 121 == sprite_val + 1):
            drawing4.append("#")
        else:
            drawing4.append(".")
    if 81 <= clock_val <= 120:
        if (clock_val-81 == sprite_val - 1) or (clock_val - 81 == sprite_val) or (clock_val - 81 == sprite_val + 1):
            drawing3.append("#")
        else:
            drawing3.append(".")
    if 41 <= clock_val <= 80:
        if (clock_val-41 == sprite_val - 1) or (clock_val - 41 == sprite_val) or (clock_val - 41 == sprite_val + 1):
            drawing2.append("#")
        else:
            drawing2.append(".")
    if 1 <= clock_val <= 40:
        if (clock_val-1 == sprite_val - 1) or (clock_val - 1 == sprite_val) or (clock_val - 1 == sprite_val + 1):
            drawing1.append("#")
        else:
            drawing1.append(".")
    return


drawing1 = []
drawing2 = []
drawing3 = []
drawing4 = []
drawing5 = []
drawing6 = []
f = open("puzzle.txt", "r")
lines = f.readlines()
clock = 0
register_x = 1
for line in lines:
    if line == "noop\n":
        clock += 1
        draw(clock, register_x)
    elif line.__contains__("addx "):
        clock += 1
        draw(clock, register_x)
        clock += 1
        draw(clock, register_x)
        tmp_sum = line.split(" ")[1].split("\n")[0]
        register_x += int(tmp_sum)
print(drawing1)
print(drawing2)
print(drawing3)
print(drawing4)
print(drawing5)
print(drawing6)
