class Directory:
    name = ""
    dir_sum = 0
    parent_directory = ""

    def __init__(self, name):
        self.name = name

    def add_sum(self, value):
        self.dir_sum += value
        return

    def get_directory(self):
        return self

    def set_parent(self, parent_directory):
        self.parent_directory = parent_directory
        return

    def get_parent(self):
        return self.parent_directory


def add_dir(directory):
    directories_list.append(directory)


def remove_dir():
    directories_list.pop(directories_list.__len__()-1)


def actual_dir():
    return directories_list[directories_list.__len__()-1]


available_space = 70_000_000
directories_list = []
root = Directory("root")
add_dir(root)
actual_directory = root
parent_dir = ""
f = open("puzzle.txt", "r")
lines = f.readlines()
for line in lines:
    if line.__contains__("$ cd .."):
        available_space -= actual_directory.dir_sum
        x = actual_directory.get_parent()
        remove_dir()
        actual_directory = actual_dir()
    elif line.__contains__("$ cd"):
        dir_name = line.split(" ")[2]
        parent_dir = actual_directory
        actual_directory = Directory(dir_name)
        add_dir(actual_directory)
        actual_directory.set_parent(parent_dir)
    elif line.__contains__("$ ls"):
        ...
    elif line.__contains__("dir "):
        ...
    else:
        actual_directory.dir_sum += int(line.split(" ")[0])

available_space -= actual_directory.dir_sum
actual_directory = directories_list[1]
available_space -= actual_directory.dir_sum
needed_space = 30_000_000 - available_space
needed_dir = 70_000_000

directories_list = []
root = Directory("root")
add_dir(root)
actual_directory = root
parent_dir = ""
f = open("puzzle.txt", "r")
lines = f.readlines()
for line in lines:
    if line.__contains__("$ cd .."):
        if needed_space <= actual_directory.dir_sum <= needed_dir:
            needed_dir = actual_directory.dir_sum
        x = actual_directory.get_parent()
        x.dir_sum += actual_directory.dir_sum
        remove_dir()
        actual_directory = actual_dir()
    elif line.__contains__("$ cd"):
        dir_name = line.split(" ")[2]
        parent_dir = actual_directory
        actual_directory = Directory(dir_name)
        add_dir(actual_directory)
        actual_directory.set_parent(parent_dir)
    elif line.__contains__("$ ls"):
        ...
    elif line.__contains__("dir "):
        ...
    else:
        actual_directory.dir_sum += int(line.split(" ")[0])

print(needed_dir)
