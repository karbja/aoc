def insertbox(row, box):
    row.append(box)
    return


def deletebox(row):
    box = row[row.__len__()-1]
    row.pop(row.__len__()-1)
    return box


def getstruct(number):
    return rowlist[number-1]


def printrow(row):
    if row.__len__() > 0:
        print(row[row.__len__()-1])
    return


row1 = ["R", "S", "L", "F", "Q"]
row2 = ["N", "Z", "Q", "G", "P", "T"]
row3 = ["S", "M", "Q", "B"]
row4 = ["T", "G", "Z", "J", "H", "C", "B", "Q"]
row5 = ["P", "H", "M", "B", "N", "F", "S"]
row6 = ["P", "C", "Q", "N", "S", "L", "V", "G"]
row7 = ["W", "C", "F"]
row8 = ["Q", "H", "G", "Z", "W", "V", "P", "M"]
row9 = ["G", "Z", "D", "L", "C", "N", "R"]

rowlist = [row1, row2, row3, row4, row5, row6, row7, row8, row9]

move = 0
move_from = 0
move_to = 0
f = open("puzzle.txt", "r")
lines = f.readlines()
for line in lines:
    move = int(line.split(" ")[1])
    move_from = int(line.split(" ")[3])
    move_from = getstruct(move_from)
    move_to = int(line.split(" ")[5].split("\n")[0])
    move_to = getstruct(move_to)
    for i in range(move):
        if move_from.__len__() > 0:
            box = deletebox(move_from)
            insertbox(move_to, box)
"""""
            [Q]     [G]     [M]
            [B] [S] [V]     [P] [R]
    [T]     [C] [F] [L]     [V] [N]
[Q] [P]     [H] [N] [S]     [W] [C]
[F] [G] [B] [J] [B] [N]     [Z] [L]
[L] [Q] [Q] [Z] [M] [Q] [F] [G] [D]
[S] [Z] [M] [G] [H] [C] [C] [H] [Z]
[R] [N] [S] [T] [P] [P] [W] [Q] [G]
 1   2   3   4   5   6   7   8   9
"""""

for i in rowlist:
    printrow(i)
