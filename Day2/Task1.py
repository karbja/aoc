def check(gameline):
    if gameline == "A X":
        return 4
    if gameline == "A Y":
        return 8
    if gameline == "A Z":
        return 3
    if gameline == "B X":
        return 1
    if gameline == "B Y":
        return 5
    if gameline == "B Z":
        return 9
    if gameline == "C X":
        return 7
    if gameline == "C Y":
        return 2
    if gameline == "C Z":
        return 6


summary = 0
f = open("Puzzle1.txt", "r")
lines = f.readlines()
for line in lines:
    line = (line.split("\n", 1))[0]
    summary += check(line)
print(summary)
