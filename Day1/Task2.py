actual_calories = 0
calories_list = []
nums_to_sum = 3
sum = 0
f = open("Puzzle1.txt", "r")
lines = f.readlines()
for line in lines:
    if len(calories_list) == nums_to_sum+1:
        calories_list.sort(reverse=True)
        calories_list.pop(nums_to_sum)
    if line != "\n":
        line = line.split("\n", 1)
        calories = int(line[0])
        actual_calories += calories
    elif line == "\n":
        calories_list.append(actual_calories)
        actual_calories = 0
        continue
for i in range(nums_to_sum):
    sum += calories_list[i]
print(sum)
