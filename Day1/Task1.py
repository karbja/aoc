max_number = 0
actual_calories = 0
f = open("Puzzle1.txt", "r")
lines = f.readlines()
for line in lines:
    if line != "\n":
        line = line.split("\n", 1)
        calories = int(line[0])
        actual_calories += calories
    elif line == "\n":
        if max_number < actual_calories:
            max_number = actual_calories
        actual_calories = 0
        continue
print(max_number)
