summary = 0
f = open("Puzzle1.txt", "r")
lines = f.readlines()
for line in lines:
    line = (line.split("\n", 1))[0]
    first_half = line[:len(line)//2]
    second_half = line[len(line)//2:]
    for letter in first_half:
        if second_half.__contains__(letter):
            if letter.isupper() is True:
                summary += ord(letter) - 38
            else:
                summary += ord(letter) - 96
            break
print(summary)
