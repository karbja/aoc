summary = 0
elves_triplet = []
f = open("Puzzle1.txt", "r")
lines = f.readlines()
for line in lines:
    elves_triplet.append(line.split("\n", 1)[0])
    if len(elves_triplet) == 3:
        for letter in elves_triplet[0]:
            if elves_triplet[1].__contains__(letter) and elves_triplet[2].__contains__(letter):
                if letter.isupper() is True:
                    summary += ord(letter) - 38
                else:
                    summary += ord(letter) - 96
                elves_triplet.clear()
                break
print(summary)
